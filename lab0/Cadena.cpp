#include <iostream>
#include <list>
#include "Aminoacido.h"
#include "Cadena.h"

using namespace std;

Cadena::Cadena(string letra){
	this->letra = letra;
}

string Cadena::get_letra(){
	return this->letra;
}

void Cadena::set_letra(string letra){
	this->letra = letra;
}

void Cadena::add_aminoacidos (Aminoacido aminoacido){
	this->aminoacidos.push_back(aminoacido);
}

list<Aminoacido> Cadena::get_aminoacidos(){
	return this->aminoacidos;
}	
