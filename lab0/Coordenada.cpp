#include <iostream>
#include <list>
#include "Coordenada.h"

Coordenada::Coordenada (){

}

Coordenada::Coordenada (float x, float y, float z){
	
	this->x = x;
	this->y = y;
	this->z = z;
}

float Coordenada::get_x() {	
	return this->x;
}

void Coordenada::set_x(float x) {
	this->x = x;
}

float Coordenada::get_y() {
	return this->y;
}

void Coordenada::set_y(float y) {
	this->y = y;
}

float Coordenada::get_z() {
	return this->z;
}

void Coordenada::set_z(float z) {
	this->z = z;
}
