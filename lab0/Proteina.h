#include <iostream>
#include <list>
#include "Cadena.h"

using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina {
	
	private:
		string nombre = "\0";
		string id = "\0";
		list<Cadena> cadenas;
		
	public:
	
		Proteina(string nombre, string id);
		void add_cadena(Cadena cadena);
		
		string get_nombre();
		void set_nombre(string nombre);
		string get_id();
		void set_id(string id);
		list<Cadena> get_cadenas();
		
};
#endif

