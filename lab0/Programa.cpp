#include <list>
#include <iostream>
#include <stdlib.h>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

using namespace std;

void imprimir_datos_proteinas(list<Proteina> proteina){
	
	for (Proteina p: proteina){
		cout << "Id: " << p.get_id() << endl;
		cout << "Nombre: " << p.get_nombre() << endl;
		for (Cadena c: p.get_cadenas()){
			cout << "Cadena: " << c.get_letra() << endl;
			for (Aminoacido a: c.get_aminoacidos()){
				cout << "Aminoacidos: " << a.get_nombre() << a.get_numero() << endl;
				for (Atomo t: a.get_atomo()){
					cout << "Atomo: " << t.get_nombre() << t.get_numero() << endl;
					cout << "Coordenada: " << t.get_coordenada().get_x() << "-" << t.get_coordenada().get_y() << "-" << t.get_coordenada().get_z() << endl;	
				}
			}
		}
	}	
}


void leer_datos_proteina(int cantidadproteinas){
	list<Proteina> proteina;
	string nproteina;
	string ncadena;
	string naminoacido;
	string id;
	string letra;
	int numeroaminoacido;
	string natomo;
	int numeroatomo;
	float coorx;
	float coory;
	float coorz;
	int cantidadcadenas;
	int cantidadaminoacidos;
	int cantidadatomos;
	
	for (int i = 1; i <= cantidadproteinas; i++){
		cout << "ingrese el nombre de la proteina: " << endl;
		cin >> nproteina;
		cout << "ingrese la ID de la proteina: " << endl;
		cin >> id;
		Proteina k = Proteina(nproteina, id);
		cout << "ingrese la cantidad de cadenas de la proteina: " << endl;
		cin >> cantidadcadenas;
		for (int z = 1; z <= cantidadcadenas; z++){
			cout << "ingrese una letra para la cadena: " << endl;
			cin >> letra;
			Cadena o = Cadena(letra);
			cout << "ingrese la cantidad de aminoacidos que debe tener la cadena: " << endl;
			cin >> cantidadaminoacidos;
			for (int w = 1; w <= cantidadaminoacidos; w++){
				cout << "ingrese el nombre del aminoacido: " << endl;
				cin >> naminoacido;
				cout << "ingrese la serie del aminacido: " << endl;
				cin >> numeroaminoacido;
				Aminoacido aminoacido = Aminoacido(naminoacido, numeroaminoacido);
				cout << "ingrese la cantidad de atomos que tendra el aminoacido: " << endl;
				cin >> cantidadatomos;
				for (int y = 1; y <= cantidadatomos; y++){
					cout << "ingrese el nombre del atomo: " << endl;
					cin >> natomo;
					cout << "ingrese el numero del atomo:" << endl;
					cin >> numeroatomo;
					cout << "ingrese la coordenada X: " << endl;
					cin >> coorx;
					cout << "ingrese la coordenada Y: " << endl;
					cin >> coory;
					cout << "ingrese la coordenada Z: " << endl;
					cin >> coorz;
					
					Coordenada coordenada = Coordenada(coorx, coory, coorz);
					Atomo atomo = Atomo(natomo, numeroatomo);
					atomo.set_coordenada(coordenada);
					aminoacido.add_atomo(atomo);
					o.add_aminoacidos(aminoacido);
					k.add_cadena(o);  
				}
			}
		}
		proteina.push_back(k);
		imprimir_datos_proteinas(proteina);
	}
}

int main(void){
	
	int cantidadproteinas;
	cout << "ingrese la cantidad de proteinas: " << endl;
	cin >> cantidadproteinas;
	leer_datos_proteina(cantidadproteinas);
	
	return 0;
}
