#include <iostream>
#include <list>
#include "Atomo.h"

using namespace std;

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido {
	
	private:
		
		string nombre = "\0";
		int numero;
		list<Atomo> atomo;
		
	public:
	
		Aminoacido(string nombre, int numero);
		
		void add_atomo(Atomo atomo);
		
		string get_nombre();
		void set_nombre(string nombre);
		int get_numero();
		void set_numero(int numero);
		list<Atomo> get_atomo();
		
};
#endif
