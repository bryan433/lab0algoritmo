#include <iostream>
#include <iostream>
#include "Coordenada.h"

using namespace std;

#ifndef ATOMO_H
#define ATOMO_H

class Atomo {
	private:
	
		string nombre = "\0";
		int numero;
		Coordenada coordenada;
	
	public:
		
		Atomo (string nombre, int numero);
		
		string get_nombre();
		void set_nombre(string nombre);
		int get_numero();
		void set_numero(int numero);
		Coordenada get_coordenada();
		void set_coordenada(Coordenada coordenada);
		
};
#endif
		
		
