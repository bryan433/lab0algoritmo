#include <iostream>
#include <list>
#include "Cadena.h"
#include "Proteina.h"

using namespace std;

Proteina::Proteina (string nombre, string id){
	this->nombre = nombre;
	this->id = id;
}

string Proteina::get_nombre(){
	return this->nombre;
}

void Proteina::set_nombre(string nombre){
	this->nombre = nombre;
}

string Proteina::get_id(){
	return this->id;
}

void Proteina::set_id(string id){
	this->id = id;
}

list<Cadena> Proteina::get_cadenas(){
	return this->cadenas;
}

void Proteina::add_cadena(Cadena cadena){
	this->cadenas.push_back(cadena);
}
