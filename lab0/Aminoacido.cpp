#include <iostream>
#include <list>
#include "Aminoacido.h"
#include "Atomo.h"

using namespace std;

Aminoacido::Aminoacido(string nombre, int numero){
	this->nombre = nombre;
	this->numero = numero;
}


string Aminoacido::get_nombre(){
	return this->nombre;
}

void Aminoacido::set_nombre(string nombre){
	this->nombre = nombre;
}

int Aminoacido::get_numero(){
	return this->numero;
}

void Aminoacido::set_numero(int numero){
	this->numero = numero;
}

list<Atomo> Aminoacido::get_atomo(){
	return this->atomo;
}

void Aminoacido::add_atomo(Atomo atomo){
	this->atomo.push_back(atomo);
}
