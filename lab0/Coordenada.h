#include <iostream>
#include <list>

using namespace std;

#ifndef COORDENADA_H
#define COORDENADA_H

class Coordenada {
	private:
	
		float x;
		float y;
		float z;
		
	public:
		
		Coordenada();
		Coordenada(float x, float y, float z);
		float get_x();
		void set_x(float x);
		float get_y();
		void set_y(float y);
		float get_z();
		void set_z(float z);
};
#endif
