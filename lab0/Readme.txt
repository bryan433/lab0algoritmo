ingreso de proteinas

Empezando el programa: Al empezar el programa nos solicitara ingresar una cantidad de proteinas, al ingresar esta cantidad usted solamente debe colocar numero ninguna letra, ya que el programa no esta optimizado para esas excepciones, luego de preguntar la cantidad de proteinas vamos a pasar a las cadenas, tambien debemos colocar la cantidad de cadenas que va a tener la proteina o tiene, y asi sucesivamente hasta terminar ingresando la cantidad de atomos y sus respectivas coordenadas de cada atomo en una dimension 3D.

Instalacion: Para la instalacion de este pograma debemos ingresar a este repositorio https://gitlab.com/bryan433/lab0algoritmo clonarlo, colocando en la terminal, git clone y el url del repositorio, luego de eso debemos entrar a la carpeta donde tenemos este archivo ingresar donde estan todos los ficheros abrir la terminal desde ahi, colocar make para compilar el programa y quedar nuestro programa fuente del que se va inicializar todo nuestro pograma, al finalizar el make, deberemos colocar este comando ./programa y se podra iniciar.

Sistema operativo:
Debian GNU/Linux 9 (stretch) 64-bit

Autor:
Bryan Ahumada Ibarra
