#include <iostream>
#include <list>
#include "Atomo.h"
#include "Coordenada.h"

using namespace std;

Atomo::Atomo (string nombre, int numero){
	this->nombre = nombre;
	this->numero = numero;
}

string Atomo::get_nombre(){
	return this->nombre;
}

void Atomo::set_nombre(string nombre){
	this->nombre = nombre;
}

int Atomo::get_numero(){
	return this->numero;
}

void Atomo::set_coordenada(Coordenada coordenada){
	this->coordenada = coordenada;
}

Coordenada Atomo::get_coordenada(){
	return this-> coordenada;
}
