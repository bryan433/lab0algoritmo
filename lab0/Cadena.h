#include <iostream>
#include <list>
#include "Aminoacido.h"

using namespace std;

#ifndef CADENA_H
#define CADENA_H

class Cadena {
	
	private:
	
		string letra = "\0";
		list<Aminoacido> aminoacidos;
	
	public:
	
		Cadena(string letra);
		void add_aminoacidos(Aminoacido aminoacidos);
		
		string get_letra();
		void set_letra(string letra);
		
		list<Aminoacido> get_aminoacidos();

};
#endif
